<?php
/**
 * A configuração de base do WordPress
 *
 * O script de criação do ficheiro wp-config.php usa este ficheiro durante
 * a instalação. Não precisa de usar o site pode copiar este ficheiro como
 * "wp-config.php" e preencher os valores
 *
 * Este ficheiro contém as seguintes variáveis:
 *
 * * Definições de MySQL
 * * Chaves secretas
 * * Prefixo das tabelas nas base de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Definições de MySQL - obtenha estes dados do seu serviço de alojamento** //
/** O nome da base de dados do WordPress */
define('DB_NAME', 'wordpress');

/** O nome do utilizador de MySQL */
define('DB_USER', 'root');

/** A password do utilizador de MySQL  */
define('DB_PASSWORD', '');

/** O nome do serviddor de  MySQL  */
define('DB_HOST', 'localhost');

/** O "Database Charset" a usar na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O "Database Collate type". Se tem dúvidas não mude. */
define('DB_COLLATE', '');

/**#@+
 * Chaves Únicas de Autenticação.
 *
 * Mude para frases únicas e diferentes!
 * Pode gerar frases automáticamente em {@link https://api.wordpress.org/secret-key/1.1/salt/ Serviço de chaves secretas de WordPress.org}
 * Pode mudar estes valores em qualquer altura para invalidar todos os cookies existentes o que terá como resultado obrigar todos os utilizadores a voltarem a fazer login
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '**sH+PsB~BWnpn)FiG?DQE=u360k&M1FjjjPY[N?va0tbh5G4B_ah,44ZiVw-K~G');
define('SECURE_AUTH_KEY',  '!_m&o=vf1@AEQr 91tt9M.YS,4L<S*~+qFo=r1j+%;UESv=%<m1TI[Ek2 7x^t}N');
define('LOGGED_IN_KEY',    'dnp[<l V7%Sydn6}{UGYfy)98tPmWxQ]#5]Ph7fe5n#N:d?IdVTJC=-SFVZ$Xcc*');
define('NONCE_KEY',        'Eb|is~vFU?8B8UU#5n, }k<F&.m;#5|%jr{mD8uW+?(V wIe7*4R`%-bTxC0=G;t');
define('AUTH_SALT',        'S8&kBXe7xAcR!&(Z<1-b^F]*c,lOTZg{L]_@(huCW3f9h,PmAzIL?LlVpi=SAy):');
define('SECURE_AUTH_SALT', 'oM,-mgp#I0i?[GSJd1}5+[@{@D}czfM;5s!*-wyxM57n*@H.]q/q_ &^:7zI@+^4');
define('LOGGED_IN_SALT',   'z-8L>?yDjj$LFzraj0?EKd8+4szBdM<kVa1V9DzxsRJ7a-<6mht%{KeZ8:]`{S04');
define('NONCE_SALT',       '~l>%,]6;XO / /6%U*&?1P5nu>U.mB#gAb4t~:`U/iBaB`]R=%CslXf|vx&.^sP!');

/**#@-*/

/**
 * Prefixo das tabelas de WordPress.
 *
 * Pode suportar múltiplas instalações numa só base de dados, ao dar a cada
 * instalação um prefixo único. Só algarismos, letras e underscores, por favor!
 */
$table_prefix  = 'wp_';

/**
 * Para developers: WordPress em modo debugging.
 *
 * Mude isto para true para mostrar avisos enquanto estiver a testar.
 * É vivamente recomendado aos autores de temas e plugins usarem WP_DEBUG
 * no seu ambiente de desenvolvimento.
 *
 * Para mais informações sobre outras constantes que pode usar,
 * visite o Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* E é tudo. Pare de editar! */

/** Caminho absoluto para a pasta do WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Define as variáveis do WordPress e ficheiros a incluir. */
require_once(ABSPATH . 'wp-settings.php');
